<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @test */
    public function test_shows_login_form()
    {
        $response = $this->get(route('login'));
        $response->assertSuccessful();

        $response->assertSee('Email');
        $response->assertSee('Password');
        $response->assertSee('Login');
    }

    /** @test */
    public function test_successful_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'secret']);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_failed_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'xxxyyy']);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_successful_logout()
    {
        $response = $this->actingAs($this->user)->post(route('logout'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_access_to_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('home'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_access_to_dashboard()
    {
        $response = $this->get(route('home'));
        $response->assertRedirect(route('login'));
    }
}
