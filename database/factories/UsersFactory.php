<?php

use Faker\Generator as Faker;

$factory->define(\App\Network\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'display_name' => $faker->userName,
        'user_name' => $faker->slug,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'remember_token' => str_random(10),
    ];
});
